import json
import time
from json import JSONDecodeError

from flask import Flask

from .machine import Machine
from .api_client import get_auth_public_key
from .logger import Logger
from .businessLogic import BusinessLogic
from .BLConsul import BLConsul

def callback(rawMsg):
    jsonMsg = json.loads(rawMsg)
    new_piece = {"piece_id": int(jsonMsg["piece_id"]), "order_id": int(jsonMsg["order_id"]), "status": "Created"}
    my_machine = Machine.get_instance()
    my_machine.add_piece_to_queue(new_piece)
    Logger.print(msg="event: piece with id " + str(jsonMsg["piece_id"]) + " in queue for order with id "+str(jsonMsg["order_id"]), service="machine",
                 level=Logger.INFO)


def callbackUpdatePubkey(msgRaw):
    jsonMsg = json.loads(msgRaw)
    BusinessLogic.get_instance().set_auth_public_key(jsonMsg["key"])

def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)

    with app.app_context():
        from . import routes

        while True:
            Logger.print(msg="Trying getting Pubkey", service="machine", level=Logger.DEBUG)
            pub_key_response = get_auth_public_key()
            if pub_key_response is None:
                Logger.print(msg="Error getting Auth Pubkey", service="machine", level=Logger.DEBUG)
                time.sleep(20)
                continue

            Logger.print(msg="Pubkey succesfully getted", service="machine", level=Logger.DEBUG)
            break

        BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"])

        from .EventHandler import EventHandler
        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)
        EventHandler(exchange="auth_pubkey", routing_key="", type="auth", callbackFunc=callbackUpdatePubkey)

        EventHandler(exchange="machine_saga", routing_key="", type="saga", callbackFunc=callback)
        BusinessLogic.get_instance().set_up_status(True)

        return app
