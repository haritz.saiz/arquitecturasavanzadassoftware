from flask import request, jsonify, abort
from flask import current_app as app
from jwt import DecodeError, ExpiredSignatureError
from werkzeug.exceptions import NotFound, InternalServerError, BadRequest, UnsupportedMediaType, Unauthorized, \
    ServiceUnavailable
import traceback
from .logger import Logger

from .businessLogic import BusinessLogic
from .machine import Machine
from .config import Config
from .BLConsul import BLConsul

config = Config.get_instance()
bl_consul = BLConsul.get_instance()

my_machine = Machine.get_instance()

# Piece Routes #########################################################################################################
@app.route('/{}/manufacture'.format(config.SERVICE_NAME), methods=['POST'])
def manufacture():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "C_MACHINE" in decoded_token["perms"]:
            if request.headers['Content-Type'] != 'application/json':
                Logger.print(msg="Unsupported media type in manufacture", service="machine", level=Logger.ERROR)
                abort(UnsupportedMediaType.code)
            content = request.json
            new_piece = None
            try:
                new_piece = {"piece_id": int(content["piece_id"]), "order_id": int(content["order_id"]),
                             "status": "Created"}
                Logger.print(msg="event: piece with id " + str(new_piece.id) + " finished",
                             service="machine",
                             level=Logger.INFO)
                my_machine.add_piece_to_queue(new_piece)
            except KeyError:
                Logger.print(msg="Bad request in manufacture", service="machine", level=Logger.ERROR)
                abort(BadRequest.code)
            response = jsonify(new_piece)
            return response
        else:
            Logger.print(msg="Not enough permissions in manufacture", service="machine", level=Logger.ERROR)
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        print("Token Error")
        Logger.print(msg="Token error in manufacture", service="machine", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/{}/health'.format(config.SERVICE_NAME), methods=['HEAD', 'GET'])
@app.route('/health', methods=['HEAD', 'GET'])
def health_check():
    if BusinessLogic.get_instance().get_up_status() == False:
        Logger.print(msg="Service unavailable", service="machine", level=Logger.ERROR)
        abort(ServiceUnavailable)

    Logger.print(msg="GET /health", service="machine", level=Logger.DEBUG)
    return "OK"
# Machine Routes #######################################################################################################

# Error Handling #######################################################################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e.__traceback__)
    return jsonify({"error_code":e.code, "error_message": e.description}), e.code


