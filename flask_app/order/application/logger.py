import datetime
import json
from .EventPublisher import EventPublisher


class Logger:
    DEBUG = "debug"
    INFO = "info"
    ERROR = "error"

    @staticmethod
    def print(msg, level, service):
        print(msg)
        ep_log = EventPublisher(exchange="log", type="topic")
        jsonMsg = {
            "service": service,
            "level": level,
            "date": str(datetime.datetime.timestamp(datetime.datetime.now())),
            "message": msg
        }
        ep_log.send_data(data=json.dumps(jsonMsg), routing_key=service + "." + level)
        ep_log.close()
