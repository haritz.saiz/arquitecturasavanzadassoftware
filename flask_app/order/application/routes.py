from flask import request, jsonify, abort
from flask import current_app as app
from jwt import DecodeError, ExpiredSignatureError

from .models import Order, Piece
from werkzeug.exceptions import NotFound, InternalServerError, BadRequest, UnsupportedMediaType, Unauthorized, \
    ServiceUnavailable
import traceback
from . import Session
from .EventPublisher import EventPublisher
from .saga_order import Saga
from .logger import Logger
from .businessLogic import BusinessLogic
from .api_client import *
import jwt
from .config import Config
from .BLConsul import BLConsul

config = Config.get_instance()
bl_consul = BLConsul.get_instance()

# Order Routes #########################################################################################################
@app.route('/{}'.format(config.SERVICE_NAME), methods=['POST'])
def create_order():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "C_ORDER" in decoded_token["perms"]:

            if request.headers['Content-Type'] != 'application/json':
                Logger.print(msg="Unsupported media type in create_order", service="order", level=Logger.ERROR)
                abort(UnsupportedMediaType.code)

            content = request.json

            if content['client_id'] != decoded_token["sub"]:
                Logger.print(msg="Impersonation attempt in create_order", service="order", level=Logger.ERROR)
                abort(Unauthorized.code)

            new_order_dict = BusinessLogic.get_instance().create_order(content['client_id'], content['number_of_pieces'])
            order = Order(
                id=new_order_dict["id"],
                number_of_pieces=new_order_dict["number_of_pieces"],
                client_id=new_order_dict['client_id'],
                status=Order.STATUS_CREATED,
                delivery_id=new_order_dict["delivery_id"],
                pieces_manufactured=new_order_dict["pieces_manufactured"],
                creation_date=new_order_dict["creation_date"],
                timestamp=new_order_dict["timestamp"],
                update_date=new_order_dict["update_date"],
            )

            Saga(order, content['address'])
            response = jsonify(new_order_dict)
            Logger.print(msg="POST /order", service="order", level=Logger.DEBUG)
            return response
        else:
            Logger.print(msg="Not enough permissions in create_order", service="order", level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        Logger.print(msg="Token error in create_order", service="order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/{}'.format(config.SERVICE_NAME), methods=['GET'])
@app.route('/orders', methods=['GET'])
def view_orders():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "U_ORDER" in decoded_token["perms"]:
            orders = BusinessLogic.get_instance().get_all_order()
            response = jsonify(Order.list_as_dict(orders))
        else:
            Logger.print(msg="Not enough permissions in view_orders", service="order", level=Logger.ERROR)
            abort(Unauthorized.code)
        Logger.print(msg="GET /order /orders", service="order", level=Logger.DEBUG)
        return response
    except (KeyError, DecodeError, ExpiredSignatureError):
        Logger.print(msg="Token error in view_orders", service="order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/{}/<int:id>'.format(config.SERVICE_NAME), methods=['GET'])
def view_order(id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_ORDER" in decoded_token["perms"]:
            order = BusinessLogic.get_instance().get_order(id)
            if order is None:
                Logger.print(msg="Not found in view_order", service="order", level=Logger.ERROR)
                abort(NotFound.code)
            elif order.client_id != decoded_token["sub"]:
                Logger.print(msg="Impersonation attempt in view_order", service="order", level=Logger.ERROR)
                abort(Unauthorized.code)
            print("GET Order {}: {}".format(id, order))
            response = jsonify(order.as_dict())
            Logger.print(msg="GET /order/<int:id>", service="order", level=Logger.DEBUG)
            return response
        else:
            Logger.print(msg="Not enough permissions in view_order", service="order", level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        Logger.print(msg="Token error in view_order", service="order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/{}/<int:id>'.format(config.SERVICE_NAME), methods=['PUT'])
def update_order(id):
   try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "U_ORDER" in decoded_token["perms"]:
            if request.headers['Content-Type'] != 'application/json':
                Logger.print(msg="Unsupported media type in update_order", service="order", level=Logger.ERROR)
                abort(UnsupportedMediaType.code)
            content = request.json
            status = content["status"]
            order_dict = BusinessLogic.get_instance().update_order(id, status)
            if order_dict is None:
                Logger.print(msg="Bad request in update_order", service="order", level=Logger.ERROR)
                abort(BadRequest.code)
            else:
                Logger.print(msg="PUT /order/<int:id>", service="order", level=Logger.DEBUG)
                response = jsonify(order_dict)

            Logger.print(msg="POST /order/<int:id>", service="order", level=Logger.DEBUG)
            return response
        else:
            Logger.print(msg="Not enough permissions in update_order", service="order", level=Logger.ERROR)
            abort(Unauthorized.code)

   except (KeyError, DecodeError, ExpiredSignatureError):
       Logger.print(msg="Token error in update_order", service="order", level=Logger.ERROR)
       abort(Unauthorized.code)

# Piece Routes #########################################################################################################

@app.route('/piece/<int:id>', methods=['PUT'])
def update_piece(id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "U_PIECE" in decoded_token["perms"]:
            if request.headers['Content-Type'] != 'application/json':
                Logger.print(msg="Unsupported media type in update_piece", service="order", level=Logger.ERROR)
                abort(UnsupportedMediaType.code)

            content = request.json
            status = content["status"]
            piece_dict = BusinessLogic.get_instance().update_piece(id, status)
            if piece_dict is None:
                Logger.print(msg="Bad request in update_piece", service="order", level=Logger.ERROR)
                abort(BadRequest.code)
            else:
                Logger.print(msg="PUT /piece/<int:id>", service="order", level=Logger.DEBUG)
                response = jsonify(piece_dict)
            return response
        else:
            Logger.print(msg="Not enough permissions in update_piece", service="order", level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        Logger.print(msg="Token error in update_piece", service="order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/piece', methods=['GET'])
@app.route('/pieces', methods=['GET'])
def view_pieces():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_PIECE" in decoded_token["perms"]:
            pieces = BusinessLogic.get_instance().get_all_pieces()
            response = jsonify(Piece.list_as_dict(pieces))
            Logger.print(msg="GET /piece /pieces", service="order", level=Logger.DEBUG)
            return response
        else:
            Logger.print(msg="Not enough permissions in view_pieces", service="order", level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        Logger.print(msg="Token error in view_pieces", service="order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/piece/<int:piece_ref>', methods=['GET'])
def view_piece(piece_ref):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_PIECE" in decoded_token["perms"]:
            piece = BusinessLogic.get_instance().get_piece(piece_ref)
            if piece is None:
                Logger.print(msg="Not found in view_piece", service="order", level=Logger.ERROR)
                abort(NotFound.code)
            Logger.print(msg="GET /piece/<int:piece_ref>", service="order", level=Logger.DEBUG)
            response = jsonify(piece.as_dict())
            return response
        else:
            Logger.print(msg="Not enough permissions in view_piece", service="order", level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        Logger.print(msg="Token error in view_piece", service="order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/order/health', methods=['HEAD', 'GET'])
@app.route('/health', methods=['HEAD', 'GET'])
def health_check():
    if BusinessLogic.get_instance().get_up_status() == False:
        Logger.print(msg="Service unavailable", service="order", level=Logger.ERROR)
        abort(ServiceUnavailable)

    Logger.print(msg="GET /health", service="order", level=Logger.DEBUG)
    return "OK"

# Error Handling #######################################################################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e.__traceback__)
    return jsonify({"error_code":e.code, "error_message": e.description}), e.code


