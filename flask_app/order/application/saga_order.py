from .saga_states import *
from .EventHandler import EventHandler
from .models import Sagas

class Saga(object):

    def on_event(self, event):
        # The next state will be the result of the on_event function.
        print("state: " + str(self.state))
        state = self.state.on_event(event)
        if state is not None:
            self.state = state

    def callback(self, body):
        print("MSG callback: " + str(body))
        jsonMsg = json.loads(body)
        self.on_event(jsonMsg["event"])

    def __init__(self, order, address):
        """ Initialize the components. """

        # Start with a default state.
        bl = BusinessLogic.get_instance()
        EventHandler(exchange="order_saga", routing_key="", type="saga", callbackFunc=self.callback)
        sagas = Sagas(order_id=order.id, status=Sagas.STATUS_ORDER_ORDERED)
        sagas_dict = bl.create_sagas(order.id, sagas)
        self.state = OrderOrderedState(order, address, sagas_dict["id"])




