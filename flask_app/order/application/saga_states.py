import json

from .logger import Logger
from .EventPublisher import EventPublisher
from .state import State
from .businessLogic import BusinessLogic
from .models import Order, Piece
from .api_client import create_piece, get_delivery_id, update_delivery_status
import datetime

eventPublisherPayment = EventPublisher("payment_saga", "fanout")
eventPublisherDelivery = EventPublisher("delivery_saga", "fanout")
eventPublisherMachine = EventPublisher("machine_saga", "fanout")


class OrderOrderedState(State):

    def __init__(self, order, address, sagas_id):
        print('Order Sagas starting')
        print('Send to payment info from order sagas')
        eventPublisherPayment.send_data(json.dumps({
            "exchange_response": "order_saga",
            "balance": order.number_of_pieces * 10,
            "client_id": order.client_id
        }), "")
        message = "SAGAS: Sagas in OrderOrderedState, starting Order with ID: " + str(order.id)
        Logger.print(msg=message, service="order", level=Logger.INFO)
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        self.order = order
        self.address = address
        self.sagas_id = sagas_id

    def on_event(self, event):
        if event == 'payment_done':
            message = "SAGAS: Sagas in OrderOrderedState, changing state of order with ID: " + str(
                self.order.id) + " to OrderPendingDeliveryState"
            Logger.print(msg=message, service="order", level=Logger.INFO)
            return OrderPendingDeliveryState(self.order, self.address, self.sagas_id)
        else:
            message = "SAGAS: Sagas in OrderOrderedState, changing state of order with ID: " + str(
                    self.order.id) + " to OrderRejectedState. Reason: NOT ENOUGH MONEY"
            Logger.print(msg=message, service="order", level=Logger.INFO)

            return OrderRejectedState(self.order, self.sagas_id)
        return self


class OrderPendingDeliveryState(State):

    def __init__(self, order, address, sagas_id):
        print('Starting delivery')
        eventPublisherDelivery.send_data(json.dumps({
            "delivery_type": "approve_order",
            "exchange_response": "order_saga",
            "address": address,
            "client_id": order.client_id,
            "order_id": order.id
        }), "")
        self.order = order
        self.sagas_id = sagas_id
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        message = "SAGAS: Sagas in OrderPendingDeliveryState, starting delivery in Order with ID: " + str(order.id)
        Logger.print(msg=message, service="order", level=Logger.INFO)

    def on_event(self, event):
        print("delivery dict: " + str(event))
        if event["status"] == 'delivery_done':
            self.order.delivery_id = event["delivery_id"]
            bl = BusinessLogic.get_instance()
            bl.update_delivery_id(self.order.id, self.order.delivery_id)
            message = "SAGAS: Sagas in OrderPendingDeliveryState, changing state of order with ID: " + str(
                    self.order.id) + " to OrderApprovedState"
            Logger.print(msg=message, service="order", level=Logger.INFO)
            return OrderApprovedState(self.order, self.sagas_id)
        else:
            message = "SAGAS: Sagas in OrderPendingDeliveryState, changing state of order with ID: " + str(
                    self.order.id) + " to OrderRefoundState. Reason: ZIP code incorrect"
            Logger.print(msg=message, service="order", level=Logger.INFO)

            return OrderRefoundState(self.order, self.sagas_id)
        return self


class OrderApprovedState(State):

    def __init__(self, order, sagas_id):
        self.order = order
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        message = "SAGAS: Sagas in OrderApprovedState, starting manufacturing of pieces in Order with ID: " + str(order.id)
        Logger.print(msg=message, service="order", level=Logger.INFO)

        for i in range(order.number_of_pieces):
            piece_dict = bl.create_piece(order.id)
            message = "SAGAS: Sagas in OrderApprovedState, starting manufacturing of piece ID: " + str(
                    piece_dict["id"]) + " in Order with ID: " + str(order.id)
            Logger.print(msg=message, service="order", level=Logger.INFO)

            eventPublisherMachine.send_data(json.dumps({
                "order_id": order.id,
                "piece_id": piece_dict["id"],
                "status": piece_dict["status"]
            }), "")

        self.sagas_id = sagas_id
        message = "SAGAS: Sagas in OrderApprovedState, manufacturing of pieces finished and Order with ID: " + str(
                order.id) + " in delivery state"
        Logger.print(msg=message, service="order", level=Logger.INFO)


class OrderRefoundState(State):

    def __init__(self, order, sagas_id):
        eventPublisherPayment.send_data(json.dumps({
            "exchange_response": "order_saga",
            "balance": -(order.number_of_pieces * 10),
            "client_id": order.client_id
        }), "")
        bl = BusinessLogic.get_instance()
        bl.update_sagas(sagas_id, self.__repr__())
        message = "SAGAS: Sagas in OrderRefoundState, starting refunding of money to auth " + str(
            order.client_id) + " in Order with ID: " + str(order.id)
        print(message)
        Logger.print(msg=message, service="order", level=Logger.INFO)

        self.order = order
        self.sagas_id = sagas_id

    def on_event(self, event):
        if event == 'payment_done':
            message = "SAGAS: Sagas in OrderRefoundState, successful refunding of money to auth " + str(
                    self.order.client_id) + " in Order with ID: " + str(self.order.id)
            Logger.print(msg=message, service="order", level=Logger.INFO)

            return OrderRejectedState(self.order, self.sagas_id)
        return self


class OrderRejectedState(State):

    def __init__(self, order, sagas_id):
        bl = BusinessLogic.get_instance()
        bl.update_order(order.id, Order.STATUS_REJECTED)
        bl.update_sagas(sagas_id, self.__repr__())
        message = "SAGAS: Sagas in OrderRejectedState, rejecting Order with ID: " + str(
            order.id) + " and delivery with ID: " + str(order.delivery_id) + " in case it was created"
        Logger.print(msg=message, service="order", level=Logger.INFO)

        # Returns NOT FOUND if the delivery is not created
        update_delivery_status(order.delivery_id, "Rejected")
        self.sagas_id = sagas_id


# NEW STATES


# End of our states.
