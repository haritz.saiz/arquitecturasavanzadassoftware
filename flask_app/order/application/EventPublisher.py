#!/usr/bin/env python
import pika
from pika import exceptions
from os import environ
from dotenv import load_dotenv
import ssl
import time

# load_dotenv()

class EventPublisher():
    def __init__(self, exchange, type):
        print("Order Publisher init")
        self.open_conection(exchange, type)

    def open_conection(self, exchange, type):
        while True:
            try:
                print("Trying rabbit connection")
                context = ssl.create_default_context(
                    cafile=environ.get("RABBITMQ_CA_CERT_LOCATION"))
                context.load_cert_chain(environ.get("RBT_CERT_LOCATION"),
                                        environ.get("RBT_KEY_LOCATION"))
                ssl_options = pika.SSLOptions(context, "rabitmq")

                self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=environ.get("RABBITMQ_IP"),
                                                                                    port=environ.get("RABBITMQ_PORT_HTTPS"),
                                                                                    ssl_options=ssl_options))
            except exceptions.AMQPConnectionError:
                print("Error connecting to RMQ")
                time.sleep(5)
                continue

            print("Connection established")
            break

        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange=exchange, exchange_type=type, durable=True)
        self.exchange = exchange
        self.type = type


    def send_data(self, data, routing_key):
        data = str(data)
        try:
            self.channel.basic_publish(exchange=self.exchange,
                                      routing_key=routing_key,
                                      body=data,
                                      properties=pika.BasicProperties(
                                      delivery_mode=2,  # make message persistent
                                  ))

            print(" [x] Sent " + data)
            print(self.exchange)
            print(routing_key)

        except exceptions.AMQPConnectionError:
            print("Rabbit MQ closed. Reopening ...")
            self.open_conection(self.exchange, self.type)
            self.send_data(data, routing_key)

    def close(self):
        self.connection.close()

