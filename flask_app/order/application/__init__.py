import sys
import time
from json import JSONDecodeError

from flask import Flask
import datetime
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine

from .logger import Logger
from .EventHandler import EventHandler
from .api_client import get_auth_public_key
from .config import Config
from .BLConsul import BLConsul


engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
Session = scoped_session(
            sessionmaker(
                autocommit=False,
                autoflush=True,
                bind=engine)
        )

from .order_performer import *
from .businessLogic import BusinessLogic

def callbackUpdatePubkey(msgRaw):
    jsonMsg = json.loads(msgRaw)
    print(jsonMsg)
    BusinessLogic.get_instance().set_auth_public_key(jsonMsg["key"])

def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    with app.app_context():
        from . import routes
        from . import models

        while True:
            message = "Trying getting Pubkey"
            Logger.print(msg=message, service="order", level=Logger.DEBUG)
            pub_key_response = get_auth_public_key()
            if pub_key_response is None:
                message = "Error getting Auth Pubkey"
                Logger.print(msg=message, service="order", level=Logger.DEBUG)
                pub_key_response = get_auth_public_key()
                time.sleep(20)
                continue

            message = "Pubkey succesfully getted"
            Logger.print(msg=message, service="order", level=Logger.DEBUG)

            break

        BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"])
        EventHandler(exchange="auth_pubkey", routing_key="", type="auth", callbackFunc=callbackUpdatePubkey)
        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)
        config = Config.get_instance()
        Logger.print(msg="The IP is "+config.IP, service="order", level=Logger.DEBUG)
        models.Base.metadata.create_all(engine)
        BusinessLogic.get_instance().set_up_status(True)

        return app

