import json

from .models import Order
from .EventPublisher import EventPublisher
from .businessLogic import BusinessLogic
from .EventHandler import EventHandler

eventPublisherDelivery = EventPublisher("delivery_performer", "fanout")

def callback(rawMsg):
    print("Order callback: " + str(rawMsg))
    jsonMsg = json.loads(rawMsg)
    event = jsonMsg["event"]
    if event == "piece_finish":
        print('event: piece_finish')
        bl = BusinessLogic.get_instance()
        piece, order = bl.update_piece(int(jsonMsg["piece_id"]), "Finished")
        if order["pieces_manufactured"] == order["number_of_pieces"]:
            print('Starting delivery 2')
            eventPublisherDelivery.send_data(json.dumps({
                "delivery_type": "finish_order",
                "delivery_id": order["delivery_id"],
                "exchange_response": "order_saga",
                "order_id": order["id"],
            }), "")
    elif event == "delivery_finish":
        print('event: delivery_finish')
        BusinessLogic.get_instance().update_order(int(jsonMsg["order_id"]), Order.STATUS_FINISHED)
    else:
        print('event: not recognized')


EventHandler(exchange="order_performer", routing_key="", type="saga", callbackFunc=callback)
