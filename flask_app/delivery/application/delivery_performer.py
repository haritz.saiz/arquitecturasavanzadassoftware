import json

from .logger import Logger
from .EventPublisher import EventPublisher
from .businessLogic import BusinessLogic
from .EventHandler import EventHandler
from .logger import Logger

eventPublisherOrder = EventPublisher("order_performer", "fanout")

def callback(rawMsg):
    print("Delivery callback: " + str(rawMsg))
    jsonMsg = json.loads(rawMsg)
    delivery_dict = BusinessLogic.get_instance().update_delivery(jsonMsg["delivery_id"], "Finished")
    Logger.print(msg="Delivery finished. OrderID: " + str(delivery_dict["order_id"]) + " DeliveryID: "+ str(jsonMsg["delivery_id"]) , service="delivery", level=Logger.INFO)
    eventPublisherOrder.send_data(json.dumps({"event": "delivery_finish", "order_id": delivery_dict["order_id"]}), "")
    Logger.print(msg="event: delivery finish for client id " + str(jsonMsg["user_id"]) + " in order id " + str(
        jsonMsg["order_id"]), service="delivery", level=Logger.INFO)


EventHandler(exchange="delivery_performer", routing_key="", type="saga", callbackFunc=callback)
