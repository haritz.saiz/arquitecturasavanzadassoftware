from flask import request, jsonify, abort
from flask import current_app as app
from jwt import DecodeError, ExpiredSignatureError

from .logger import Logger
from .businessLogic import BusinessLogic
from .models import Delivery
from werkzeug.exceptions import NotFound, InternalServerError, BadRequest, UnsupportedMediaType, Unauthorized, \
    ServiceUnavailable
import traceback
from . import Session
from .api_client import update_order

from .EventPublisher import EventPublisher
from .logger import Logger
from .config import Config
from .BLConsul import BLConsul

config = Config.get_instance()
bl_consul = BLConsul.get_instance()

# Delivery Routes #########################################################################################################
@app.route('/{}'.format(config.SERVICE_NAME), methods=['POST'])
def create_delivery():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "C_DELIVERY" in decoded_token["perms"]:
            if request.headers['Content-Type'] != 'application/json':
                Logger.print(msg="Unsupported media type in create_delivery", service="delivery", level=Logger.ERROR)
                abort(UnsupportedMediaType.code)
            content = request.json

            new_delivery_dict = BusinessLogic.get_instance().create_delivery(order_id=content['order_id'],
                                                                             user_id=content['user_id'],
                                                                             address=content['address'])
            if new_delivery_dict is None:
                Logger.print(msg="Not found in create_delivery", service="delivery", level=Logger.ERROR)
                abort(NotFound.code)
            else:
                Logger.print(msg="POST /delivery", service="delivery", level=Logger.DEBUG)
                response = jsonify(new_delivery_dict)
            return response
        else:
            Logger.print(msg="Not enough permissions in create_delivery", service="delivery", level=Logger.ERROR)
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        print("Token Error")
        Logger.print(msg="Token error in create_delivery", service="delivery", level=Logger.ERROR)
        abort(Unauthorized.code)



@app.route('/{}/<int:delivery_id>'.format(config.SERVICE_NAME), methods=['GET'])
def view_delivery(delivery_id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_DELIVERY" in decoded_token["perms"]:
            delivery = BusinessLogic.get_instance().get_delivery(delivery_id)
            if delivery.user_id != decoded_token["sub"]:
                Logger.print(msg="Impersonation attempt in view_delivery", service="delivery", level=Logger.ERROR)
                abort(Unauthorized.code)
            if delivery is None:
                Logger.print(msg="Not found in view_delivery", service="delivery", level=Logger.ERROR)
                abort(NotFound.code)
            print("GET Client {}: {}".format(delivery_id, delivery))
            Logger.print(msg="GET /delivery/<int:delivery_id>", service="delivery", level=Logger.DEBUG)
            response = jsonify(delivery.as_dict())
            return response
        else:
            Logger.print(msg="Not enough permissions in view_delivery", service="delivery", level=Logger.ERROR)
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        print("Token Error")
        Logger.print(msg="Token error in view_delivery", service="delivery", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/{}/<int:delivery_id>'.format(config.SERVICE_NAME), methods=['PUT'])
def update_delivery(delivery_id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "U_DELIVERY" in decoded_token["perms"]:
            if request.headers['Content-Type'] != 'application/json':
                Logger.print(msg="Unsupported media type in update_delivery", service="delivery", level=Logger.ERROR)
                abort(UnsupportedMediaType.code)
            content = request.json
            status = content['status']
            delivery_dict = BusinessLogic.get_instance().update_delivery(delivery_id, status)
            if delivery_dict is None:
                Logger.print(msg="Not found in update_delivery", service="delivery", level=Logger.ERROR)
                abort(NotFound.code)
            if status == Delivery.STATUS_FINISHED:
                update_order(delivery_dict["order_id"], "Delivered")

            Logger.print(msg="PUT /delivery/<int:delivery_id>", service="delivery", level=Logger.DEBUG)
            response = jsonify(delivery_dict)
            return response
        else:
            Logger.print(msg="Not enough permissions in update_delivery", service="delivery", level=Logger.ERROR)
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        print("Token Error")
        Logger.print(msg="Token error in view_delivery", service="delivery", level=Logger.ERROR)
        abort(Unauthorized.code)



@app.route('/{}/order/<int:order_id>'.format(config.SERVICE_NAME), methods=['GET'])
def view_order_delivery(order_id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_DELIVERY" in decoded_token["perms"]:
            delivery = BusinessLogic.get_instance().get_order_delivery(order_id)
            if delivery is None:
                Logger.print(msg="Not found in view_order_delivery", service="delivery", level=Logger.ERROR)
                abort(NotFound.code)
            Logger.print(msg="PUT /delivery/order/<int:order_id>", service="delivery", level=Logger.DEBUG)
            response = jsonify(delivery.as_dict())
            print(response)
            Logger.print(msg="GET /delivery/order/<int:order_id>", service="delivery", level=Logger.DEBUG)
            return response
        else:
            Logger.print(msg="Not enough permissions in view_order_delivery", service="delivery", level=Logger.ERROR)
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        print("Token Error")
        Logger.print(msg="Token error in view_order_delivery", service="delivery", level=Logger.ERROR)
        abort(Unauthorized.code)

@app.route('/{}/health'.format(config.SERVICE_NAME), methods=['HEAD', 'GET'])
@app.route('/health', methods=['HEAD', 'GET'])
def health_check():
    if BusinessLogic.get_instance().get_up_status() == False:
        Logger.print(msg="Service unavailable", service="delivery", level=Logger.ERROR)
        abort(ServiceUnavailable)

    Logger.print(msg="GET /health", service="delivery", level=Logger.DEBUG)
    return "OK"

# Error Handling #######################################################################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e.__traceback__)
    return jsonify({"error_code":e.code, "error_message": e.description}), e.code


