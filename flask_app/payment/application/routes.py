from flask import request, jsonify, abort
from flask import current_app as app
from jwt import DecodeError, ExpiredSignatureError
from werkzeug.exceptions import NotFound, InternalServerError, BadRequest, UnsupportedMediaType, HTTPException, \
    NotAcceptable, Unauthorized, ServiceUnavailable
import traceback
from .businessLogic import BusinessLogic
from .EventPublisher import EventPublisher
from .logger import Logger
from .config import Config
from .BLConsul import BLConsul

config = Config.get_instance()
bl_consul = BLConsul.get_instance()

eventPublisher = EventPublisher("payment", "fanout")

# Payment Routes #########################################################################################################
@app.route('/{}'.format(config.SERVICE_NAME), methods=['POST'])
def create_payment():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "C_PAYMENT" in decoded_token["perms"]:
            if request.headers['Content-Type'] != 'application/json':
                Logger.print(msg="Unsupported media type in create_payment", service="payment", level=Logger.ERROR)
                abort(UnsupportedMediaType.code)
            content = request.json
            if content["user_id"] != decoded_token["sub"]:
                Logger.print(msg="Impersonation attempt in create_payment", service="payment", level=Logger.ERROR)
                abort(Unauthorized.code)
            new_payment_dict = BusinessLogic.get_instance().create_payment(content['user_id'], content['balance'])
            if new_payment_dict is None:
                Logger.print(msg="Bad request in create_payment", service="payment", level=Logger.ERROR)
                abort(BadRequest.code)
            else:
                eventPublisher.send_data(data="POST new Payment created: " + str(new_payment_dict), routing_key="")
                response = jsonify(new_payment_dict)
            Logger.print(msg="POST /payment", service="payment", level=Logger.DEBUG)
            return response
        else:
            Logger.print(msg="Not enough permissions in create_payment", service="payment", level=Logger.ERROR)
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        print("Token Error")
        Logger.print(msg="Token error in create_payment", service="payment", level=Logger.ERROR)
        abort(Unauthorized.code)



@app.route('/{}/<string:user_id>'.format(config.SERVICE_NAME), methods=['GET'])
def view_payment(user_id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_DELIVERY" in decoded_token["perms"]:
            payment = BusinessLogic.get_instance().get_payment(user_id)
            if user_id != decoded_token["sub"]:
                Logger.print(msg="Impersonation attempt in view_payment", service="payment", level=Logger.ERROR)
                abort(Unauthorized.code)
            if not payment:
                Logger.print(msg="Not found in view_payment", service="payment", level=Logger.ERROR)
                abort(NotFound.code)
            print("GET Client {}: {}".format(user_id, payment))
            response = jsonify(payment.as_dict())
            Logger.print(msg="GET /payment/<string:user_id>", service="payment", level=Logger.DEBUG)
            return response
        else:
            Logger.print(msg="Not enough permissions in view_payment", service="payment", level=Logger.ERROR)
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        print("Token Error")
        Logger.print(msg="Toke error in view_payment", service="payment", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/{}/<string:user_id>'.format(config.SERVICE_NAME), methods=['PUT'])
def update_balance(user_id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "U_PAYMENT" in decoded_token["perms"]:
            if request.headers['Content-Type'] != 'application/json':
                abort(UnsupportedMediaType.code)
            content = request.json
            if user_id != decoded_token["sub"]:
                Logger.print(msg="Impersonation attempt in update_balance", service="payment", level=Logger.ERROR)
                abort(Unauthorized.code)
            payment = BusinessLogic.get_instance().update_payment(user_id, content['balance'])
            if payment is None:
                eventPublisher.send_data(data="PUT_ERROR changing Payment status", routing_key="")
                Logger.print(msg="Not found in update_balance", service="payment", level=Logger.ERROR)
                abort(NotFound.code)
            else:
                eventPublisher.send_data(data="PUT Payment status changed to: " + str(payment.as_dict()),
                                         routing_key="")

            Logger.print(msg="PUT /payment/<string:user_id>", service="payment", level=Logger.DEBUG)
            response = jsonify(payment.as_dict())
            return response
        else:
            Logger.print(msg="Not enough permissions in update_balance", service="payment", level=Logger.ERROR)
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        print("Token Error")
        Logger.print(msg="Toke error in update_balance", service="payment", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route('/health', methods=['HEAD', 'GET'])
@app.route('/{}/health'.format(config.SERVICE_NAME), methods=['HEAD', 'GET'])
def health_check():
    if BusinessLogic.get_instance().get_up_status() == False:
        Logger.print(msg="Service unavailable", service="payment", level=Logger.ERROR)
        abort(ServiceUnavailable)

    Logger.print(msg="GET /health", service="payment", level=Logger.DEBUG)
    return "OK"

# Error Handling #######################################################################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    traceback.print_tb(e.__traceback__)
    return jsonify({"error_code":e.code, "error_message": e.description}), e.code

class PaymentRequired(HTTPException):
    code = 402
    description = '<p>Payment required.</p>'
