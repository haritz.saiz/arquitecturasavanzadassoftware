#!/usr/bin/env python
import time

import pika
from os import environ
from dotenv import load_dotenv
import json

from pika import exceptions

from .businessLogic import BusinessLogic
from .EventPublisher import EventPublisher
from threading import Thread
import ssl

# load_dotenv()

class EventHandler(Thread):

    def __init__(self, exchange, routing_key, type, callbackFunc):
        Thread.__init__(self)
        while True:
            try:
                print("Trying rabbit connection")

                context = ssl.create_default_context(
                    cafile=environ.get("RABBITMQ_CA_CERT_LOCATION"))
                context.load_cert_chain(environ.get("RBT_CERT_LOCATION"),
                                        environ.get("RBT_KEY_LOCATION"))
                ssl_options = pika.SSLOptions(context, "rabitmq")

                self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=environ.get("RABBITMQ_IP"),
                                                                                    port=environ.get(
                                                                                        "RABBITMQ_PORT_HTTPS"),
                                                                                    ssl_options=ssl_options))
            except exceptions.AMQPConnectionError:
                print("Error connecting to RMQ")
                time.sleep(5)
                continue

            print("Connection established")
            break

        self.callbackFunc = callbackFunc
        self.channel = self.connection.channel()
        self.type = type
        self.channel.exchange_declare(exchange=exchange, exchange_type='fanout', durable=True)
        result = self.channel.queue_declare(queue='', durable=True)
        self.channel.queue_bind(exchange=exchange, queue=result.method.queue, routing_key=routing_key)
        self.channel.basic_consume(queue=result.method.queue,
                      auto_ack=False,
                      on_message_callback=self.callback)
        self.start()

    def run (self):
        print(' [*] Waiting for messages. To exit press CTRL+C')
        self.channel.start_consuming()

    def callback(self, ch, method, properties, body):
        print(" [x] Received %r" % body)
        self.callbackFunc(body)
        ch.basic_ack(delivery_tag = method.delivery_tag)


