from .models import Log
from . import Session
import jwt


#eventPublisher = EventPublisher("payment")

class BusinessLogic():
    __instance = None
    __public_key = None
    __up_status = False

    def __init__(self):
        if BusinessLogic.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            BusinessLogic.__instance = self

    @staticmethod
    def get_instance():
        if BusinessLogic.__instance is None:
            BusinessLogic()
        return BusinessLogic.__instance

    def decrypt_jwt(self, auth_header):
        args = auth_header.split("Bearer ")
        jwt_token = args[1]
        decoded_token = jwt.decode(jwt_token, BusinessLogic.__public_key, algorithms='RS256')
        return decoded_token

    def set_auth_public_key(self, pubkey):
        BusinessLogic.__public_key = pubkey

    def get_auth_public_key(self):
        return BusinessLogic.__public_key

    def set_up_status(self, status):
        BusinessLogic.__up_status = status

    def get_up_status(self):
        return BusinessLogic.__up_status

    def get_all_logs(self):
        session = Session()
        logs = session.query(Log).all()
        session.close()
        return logs

    def create_log(self, service, level, date, message):
        session = Session()
        try:
            log = Log(service=service, level=level, date=date, message=message)
            session.add(log)
            session.commit()
            session.close()
        except KeyError:
            session.rollback()
            session.close()

    def get_log_by_level(self, level):
        session = Session()
        logs = session.query(Log).filter(Log.level == level).all()
        session.close()
        return logs