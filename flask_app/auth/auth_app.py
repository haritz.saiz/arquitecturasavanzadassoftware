from application import create_app
from application.config import Config
# Creamos las claves publicas y privadas
from Crypto.PublicKey import RSA

key = RSA.generate(2048)

secret_code = "Monolitics"
encrypted_key = key.export_key(passphrase=secret_code, pkcs=8,
                              protection="scryptAndAES128-CBC")

file_out = open("rsa_key.pem", "wb")
file_out.write(key.exportKey('PEM'))
file_out.close()

app = create_app()
app.app_context().push()

if __name__ == "__main__":
    config = Config.get_instance()
    app.run(host=config.IP, port=config.PORT, ssl_context=('aas_auth_ssl.crt', 'aas_auth_ssl.key'))
